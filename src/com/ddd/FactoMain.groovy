package com.ddd

class FactoMain {

    static main(args) {
        PriceFactory priceFactory = new PriceFactory()
        
        Price price1 = priceFactory.createPrice("12 euros")
        Price price2 = priceFactory.createPrice("10 pounds")
        Price price3 = priceFactory.createPrice("12 euros")
        
        assert price1 != price2
        assert price2 != price3
        assert price1 == price3
        
        ProductFactory productFactory = new ProductFactory()
        println productFactory.createProduct("Bertha1 15 dollars")
    }

}
