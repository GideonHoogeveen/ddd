package com.ddd

class Database {
    
    public static Collection<Product> productTable = new HashSet<>()
    
    public static void insert(Product product) {
        product.id = UUID.randomUUID()
        productTable.add(product.clone())
    }
    
    public static void delete(Product product) {
        productTable.remove(product)
    }
    
    public static Product get(Closure closure) {
        productTable.find(closure)?.clone()
    }
    
    public static printToConsole() {
        println "Database: ${productTable}"
    }
    
}
