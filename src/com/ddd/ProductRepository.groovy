package com.ddd

class ProductRepository {
    
    public void save(Product product) {
        Database.insert(product)
    }
    
    public void delete(Product product) {
        Database.delete(product)
    }
    
    public Product getByName(String name) {
        Database.get {Product product -> product.name == name}
    }
    
    public Product getByPrice(Price price) {
        Database.get {Product product -> product.price == price}
    }
    
}
