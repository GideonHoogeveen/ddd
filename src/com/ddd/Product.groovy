package com.ddd

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode(includes='id') // equals and hashcode on id only
class Product {
    String id
    String name
    Price price
    
    public Product(String name, Price price) {
        this.name = name
        this.price = price
    }
    
    private Product(Product other) {
        this.id = other.id
        this.name = other.name
        this.price = other.price
    }
    
    public Product clone() {
        return new Product(this)
    }

    @Override
    public String toString() {
        return "(${id}, ${name}, ${price})";
    }    
    
}
