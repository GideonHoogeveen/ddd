package com.ddd

import groovy.transform.Immutable;

@Immutable //maakt alle properties immutable, genereert een tuple constructor en genereert een hascode & equals methode
class Price {
    
    int amount
    Currency currency
    @Override
    public String toString() {
        return "(${amount}, ${currency})";
    }
    
}
