package com.ddd

class ProductFactory {
    
    public Product createProduct(String input) {
        String[] splitted = input.split(' ')
        
        if(splitted.length != 3) {
            return null
        }
        
        String name = splitted[0]
            
        if(!splitted[1].isInteger()) {
            return null
        }
        int amount = splitted[1].toInteger()
        Price price
        switch(splitted[2].toLowerCase()) {
            case "euros":
                price = new Price(amount, Currency.EUROS)
                break;
            case "dollars":
                price = new Price(amount, Currency.DOLLARS)
                break;
            case "pounds":
                price = new Price(amount, Currency.POUNDS)
                break;
            default:
                return null
        }
        return new Product(name, price)
    }
    
}
