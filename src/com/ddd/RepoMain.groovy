package com.ddd

import groovy.transform.CompileStatic;

@CompileStatic
class RepoMain {

    static main(args) {
        ProductFactory productFactory = new ProductFactory()
        ProductRepository productRepository = new ProductRepository()
               
        Product product = productFactory.createProduct("Frisbee 2 euros")
        println product
        Database.printToConsole()
        productRepository.save(product)
        println product
        Database.printToConsole()
        
        Product retreivedProduct = productRepository.getByName('Frisbee')
        
        println "get by name: ${productRepository.getByName('Frisbee')}"
        
        retreivedProduct.name = "Henk"
        productRepository.delete(retreivedProduct)
        
        Database.printToConsole()
    }

}
