package com.ddd

class PriceFactory {
    
    /**
     * Creates a price based on a String, i.e. "12 euros"
     * @param input 
     */
    public Price createPrice(String input) {
        String[] splitted = input.split(' ')
        
        if(splitted.length != 2 || !splitted[0].isInteger()) {
            return null
        }
        int amount = splitted[0].toInteger()
        
        switch(splitted[1].toLowerCase()) {
            case "euros":
                return new Price(amount, Currency.EUROS)
            case "dollars":
                return new Price(amount, Currency.DOLLARS)
            case "pounds":
                return new Price(amount, Currency.POUNDS)
            default:
                return null
        }
    }
    
}
